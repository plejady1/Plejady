############################################################################
# 
# TEAM Plejády
#
# Základni skola Kolin 
# 
#       K  K     OOO    L     IIIII N     N
#       K K     O   O   L       I   N N   N
#       Kk      O   O   L       I   N  N  N
#       k K     O   O   L       I   N   N N
#       k  K     OOO    LLLLL IIIII N     N  
# 
# 
# 
# MISSION SPACE LAB 2021 - 2022 
# 
############################################################################



from pathlib import Path
from logzero import logger, logfile
from sense_hat import SenseHat
from picamera import PiCamera
from orbit import ISS
from time import sleep
from datetime import datetime, timedelta
from gpiozero import CPUTemperature
from skyfield.api import load


import csv

ephemeris = load('de421.bsp')
timescale = load.timescale()


#   Založení CSV souboru a zápis hlavičky
#   Create a new CSV file and add the header
def create_csv_file(data_file):
    """Create a new CSV file and add the header row"""
    with open(data_file, 'w') as f:
        writer = csv.writer(f)
        header = ("__cykl", "_________Datum_ Čas", "__Den:Noc",\
         "____Latitude", "___Longitude", "___Average", "__CPU Temp", "___Teplota", "__Vlhkost",\
         "__Tlak", "__pitch", "roll", "yaw",\
         "__magX", "__magY", "__magZ",\
         "__accX", "__accY", "__accZ",\
         "__gyroX", "__gyroY", "__gyroZ")
        writer.writerow(header)

#   Otevře CSV souboru a zápis věty
#   Add a row of data to the data_file CSV"""    

def add_csv_data(data_file, data):
    """Add a row of data to the data_file CSV"""
    with open(data_file, 'a') as f:
        writer = csv.writer(f)
        writer.writerow(data)

def convert(angle):
    """
    Convert a `skyfield` Angle to an EXIF-appropriate
    representation (rationals)
    e.g. 98° 34' 58.7 to "98/1,34/1,587/10"

    Return a tuple containing a boolean and the converted angle,
    with the boolean indicating if the angle is negative.
    """
    sign, degrees, minutes, seconds = angle.signed_dms()
    exif_angle = f'{degrees:.0f}/1,{minutes:.0f}/1,{seconds*10:.0f}/10'
    return sign < 0, exif_angle



base_folder = Path(__file__).parent.resolve()

# Set a logfile name
logfile(base_folder/"Plejady.log")

# Set up Sense Hat
sense = SenseHat()
sense.clear()
sense.low_light = True


# Initialise the CSV file
data_file = base_folder/"Plejady.csv"
create_csv_file(data_file)
##############################################################################
# Sets RGB matrix related variables and functions
# sets RGB color values
R = (255, 0, 0)
G = (0, 255, 0)
B = (0, 0, 255)
O = (0, 0, 0)# sets RGB values for black (OFF)

def hdd(): # sets icons for writing time (green arrow)
    sense.show_message("4 ZS Kolin")
    logo = [
    B, B, B, B, B, B, B, B,
    B, O, G, G, G, G, O, B,
    B, O, G, G, G, G, O, B,
    B, O, G, G, G, G, O, B,
    B, G, G, G, G, G, G, B,
    B, O, G, G, G, G, O, B,
    B, O, O, G, G, O, O, B,
    B, B, B, B, B, B, B, B,
    ]
    return logo

def wait1():  # sets icons for waiting time (sand clock 1)
    logo = [
    B, B, B, B, B, B, B, B,
    B, R, R, R, R, R, R, B,
    O, B, R, R, R, R, B, O,
    O, O, B, R, R, B, O, O,
    O, O, B, O, O, B, O, O,
    O, B, O, O, O, O, B, O,
    B, O, O, O, O, O, O, B,
    B, B, B, B, B, B, B, B,
    ]
    return logo

def wait2():  # sets icons for waiting time (sand clock 2)
    logo = [
    B, B, B, B, B, B, B, B,
    B, R, R, O, O, R, R, B,
    O, B, R, R, R, R, B, O,
    O, O, B, R, R, B, O, O,
    O, O, B, R, R, B, O, O,
    O, B, O, O, O, O, B, O,
    B, O, O, O, O, O, O, B,
    B, B, B, B, B, B, B, B,
    ]
    return logo

def wait3():  # sets icons for waiting time (sand clock 3)
    logo = [
    B, B, B, B, B, B, B, B,
    B, R, O, O, O, O, R, B,
    O, B, R, R, R, R, B, O,
    O, O, B, R, R, B, O, O,
    O, O, B, R, R, B, O, O,
    O, B, O, R, R, O, B, O,
    B, O, O, O, O, O, O, B,
    B, B, B, B, B, B, B, B,
    ]
    return logo

def wait4():  # sets icons for waiting time (sand clock 4)
    logo = [
    B, B, B, B, B, B, B, B,
    B, O, O, O, O, O, O, B,
    O, B, R, R, R, R, B, O,
    O, O, B, R, R, B, O, O,
    O, O, B, R, R, B, O, O,
    O, B, O, R, R, O, B, O,
    B, O, O, R, R, O, O, B,
    B, B, B, B, B, B, B, B,
    ]
    return logo

def wait5():  # sets icons for waiting time (sand clock 5)
    logo = [
    B, B, B, B, B, B, B, B,
    B, O, O, O, O, O, O, B,
    O, B, R, O, O, R, B, O,
    O, O, B, R, R, B, O, O,
    O, O, B, R, R, B, O, O,
    O, B, O, R, R, O, B, O,
    B, O, R, R, R, R, O, B,
    B, B, B, B, B, B, B, B,
    ]
    return logo

def wait6():  # sets icons for waiting time (sand clock 6)
    logo = [
    B, B, B, B, B, B, B, B,
    B, O, O, O, O, O, O, B,
    O, B, O, O, O, O, B, O,
    O, O, B, R, R, B, O, O,
    O, O, B, R, R, B, O, O,
    O, B, O, R, R, O, B, O,
    B, R, R, R, R, R, R, B,
    B, B, B, B, B, B, B, B,
    ]
    return logo

def wait7():  # sets icons for waiting time (sand clock 7)
    logo = [
    B, B, B, B, B, B, B, B,
    B, O, O, O, O, O, O, B,
    O, B, O, O, O, O, B, O,
    O, O, B, O, O, B, O, O,
    O, O, B, R, R, B, O, O,
    O, B, R, R, R, R, B, O,
    B, R, R, R, R, R, R, B,
    B, B, B, B, B, B, B, B,
    ]
    return logo

def done():  # sets icon for end of program (green checkmark)
    logo = [
    O, O, O, O, O, O, O, G, 
    O, O, O, O, O, O, G, G,
    O, O, O, O, O, G, G, G,
    G, G, O, O, G, G, G, O,
    G, G, G, G, G, G, O, O,
    O, G, G, G, G, O, O, O,
    O, O, G, G, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]
    return logo

# Sets icons sequence in a list
icons = [wait1, wait2, wait3, wait4, wait5, wait6, wait7]

# Initialise the photo counter
counter = 1
# Record the start and current time
start_time = datetime.now()

stop_time = start_time + timedelta(minutes=176) # 3 hodiny - 4 minuty
# stop_time = start_time + timedelta(minutes=5)

now_time = datetime.now()
Zbytek_time = stop_time - now_time
# Run a loop for (almost) three hours
while (now_time < stop_time):
    try:
        print(f'Now_time______________ {now_time}.')
        print(f'zbytek_time __________ {Zbytek_time}.')
        print(f'start_time ___________ {start_time}.')
        print(f'stop_time ____________ {stop_time}.')
        t = timescale.now()
        # Record the start and current time
        now_time = datetime.now()
        Zbytek_time = stop_time - now_time
        #
        Fotka = f"Plejady_{counter:03d}"
        print (Fotka)
        if ISS.at(t).is_sunlit(ephemeris):
            Den_Noc = "Den"
            print (Den_Noc)
            print (t)
            print (now_time)
            # Capture image
            # capture(cam, image_file)
        else:
            Den_Noc = "Noc"
            print(Den_Noc)
            print (t)
            print (now_time)
            Fotka = "Noc"
        #
        # Next Calculate
        # 
        #
        #########################################
        #
        #Senzory SETUP
        #
        #########################################
        sense.set_pixels(hdd()) # displays writing on memory icon (green arrow)

        sleep(2)
        o = sense.get_orientation() # writes orientation data from gyroscope
        pitch = round(o["pitch"], 4)
        roll = round(o["roll"], 4)
        yaw = round(o["yaw"], 4)

        mag = sense.get_compass_raw() # writes data from magnetometer
        magX = round(mag["x"], 4)
        magY = round(mag["y"], 4)
        magZ = round(mag["z"], 4)

        acc = sense.get_accelerometer_raw() # writes data from accelerometer
        accX = round(acc["x"], 4)
        accY = round(acc["y"], 4)
        accZ = round(acc["z"], 4)

        gyro = sense.get_gyroscope_raw() # writes data from gyroscope
        gyroX = round(gyro["x"], 4)
        gyroY = round(gyro["y"], 4)
        gyroZ = round(gyro["z"], 4)

        #################################################################x

        humidity = round(sense.humidity, 4)
        temperature = round(sense.temperature, 4)
        pressure = round(sense.pressure, 4)
        cpu = CPUTemperature()
        print(cpu.temperature)
        print(cpu)
        print(counter)
        # Get coordinates of location on Earth below the ISS
        location = ISS.coordinates()
        # Save the data to the file
        #sleep(30)

        data = (
            counter,
            datetime.now(),
            Den_Noc,
            location.latitude.degrees,
            location.longitude.degrees,
            location.elevation.km,
            cpu.temperature,
            temperature,
            humidity,
            pressure,
            pitch, roll, yaw, magX, magY, magZ,\
            accX, accY, accZ, gyroX, gyroY

        )
        ###############################
        #data = (
        #    date1, time1, sample_counter,\
        ##    lat_deg, long_deg, sun_angle_deg, day_or_night,\
        #    iss.sublat, iss.sublong,\
        #    cpu_temp, temp, pres, hum,\
        #    pitch, roll, yaw, magX, magY, magZ,\
        #    accX, accY, accZ, gyroX, gyroY, gyroZ        
        #)



        add_csv_data(data_file, data)
        # Capture image
        #image_file = f"{base_folder}/photo_{counter:03d}.jpg"
        #capture(cam, image_file)
        # Log event
        print(location.latitude.degrees)
        print(location.longitude.degrees)
        print(location.elevation.km)
        ###############################################################################################
        #
        #

        t = load.timescale().now()
        # Compute where the ISS is at time `t`
        position = ISS.at(t)
        # Compute the coordinates of the Earth location directly beneath the ISS
        location1 = position.subpoint()
        print(location1)


        #Pokud nemáte zájem o nastavení nebo záznam času t, 
        # pak ISSobjekt nabízí i pohodlnou coordinatesmetodu, 
        # kterou můžete použít jako alternativu pro získání souřadnic místa na Zemi, 
        # které se aktuálně nachází přímo pod ISS:

        from orbit import ISS
        location = ISS.coordinates() # Equivalent to ISS.at(timescale.now()).subpoint()
        #print(location)

        #Poznámka : Aktuální poloha ISS je odhadem na základě telemetrických dat a aktuálního času. 
        # Proto, když testujete svůj program na Desktop Flight OS, musíte se ujistit, že byl správně nastaven systémový čas.

        #Je locationto také GeographicPosition, 
        # takže se můžete podívat do dokumentace 
        # a zjistit, jak získáte přístup k jejím jednotlivým prvkům :

        print(f'Latitude: {location.latitude}')
        print(f'Longitude: {location.longitude}')
        print(f'Elevation: {location.elevation.km}')

        #print(f'Latitude: {location.latitude}')
        #print(f'Longitude: {location.longitude}')
        #print(f'Elevation: {location.elevation.km}'
        #
        #
        ###############################################################################################
        logger.info(f"iteration {counter}")
        counter += 1
        sleep(2)  
        #sense = SenseHat()
        #sense.show_letter("4 ZS Kolin")   
        for x in range (0,7):
            sense.set_pixels(icons[x]())
            sleep(1)
     
       
        # Update the current time
        now_time = datetime.now()
    except Exception as e:
        logger.error(f'{e.__class__.__name__}: {e}')

####################################################x
sense.set_pixels(done()) # displays green checkmark

# Shows green checkmark for 10 seconds and clears matrix afterwards
sleep(10)
sense.clear()